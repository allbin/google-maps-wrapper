"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var proj4_1 = require("proj4");
var ScriptCache_1 = require("./ScriptCache");
var ScissorIcon = require('./img/marker_scissors.svg');
var ScissorHoverIcon = require('./img/marker_scissors_hover.svg');
var DEFAULT_POLYLINE_OPTIONS = {
    visible: true
};
var DEFAULT_POLYGON_OPTIONS = {
    visible: true
};
var DEFAULT_MARKER_OPTIONS = {
    visible: true
};
var DEFAULT_DIRECTIONS_OPTIONS = {
    visible: true,
    hideRouteList: true,
    suppressInfoWindows: true,
    preserveViewport: true
};
var CUTTING_SNAP_DISTANCE = 200;
var Z_INDEX_HIGHLIGHTS = 8999; //The highlight-objects z-index.
var Z_INDEX_HIGHLIGHTED = 9000; //The original objects z-index during highlighten.
var Z_INDEX_SCISSORS = 9001;
var Z_INDEX_SCISSORS_HOVER = 9002;
var EARTH_RADIUS = 6378137;
var PROJECTIONS = {
    gmaps: '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0.0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs +over',
    rt90: '+proj=tmerc +lat_0=0 +lon_0=15.80827777777778 +k=1 +x_0=1500000 +y_0=0 +ellps=bessel +towgs84=414.1,41.3,603.1,-0.855,2.141,-7.023,0 +units=m +no_defs',
    sweref99: '+proj=tmerc +lat_0=0 +lon_0=15.80628452944445 +k=1.00000561024 +x_0=1500064.274 +y_0=-667.711 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
};
proj4_1.default.defs('GMAPS', PROJECTIONS.gmaps);
proj4_1.default.defs('RT90', PROJECTIONS.rt90);
proj4_1.default.defs('SWEREF99', PROJECTIONS.sweref99);
var WrappedMapBase = /** @class */ (function (_super) {
    __extends(WrappedMapBase, _super);
    function WrappedMapBase(props) {
        var _this = _super.call(this, props) || this;
        _this.map = null;
        _this.initialized = false;
        _this.map_objects = {
            directions: {},
            marker: {},
            polygon: {},
            polyline: {},
        };
        _this.highlight_objects = {
            marker: {},
            polygon: {},
            polyline: {}
        };
        _this.cutting_objects = {};
        _this.do_after_init = [];
        _this.do_on_drag_end = [];
        _this.do_on_drag_start = [];
        _this.overlay = null;
        _this.cutting = {
            enabled: false,
            id: null,
            indexes: null,
        };
        _this.helpers = {
            rt90: {
                pointsAroundCircle: makePointsAroundCircleRT90,
                makeRect: makeRectRT90,
                arrayRT90ToWGS84: function (rt90arr) { return convert("RT90", "WGS84", rt90arr); },
                arrayRT90ToLatLngObj: function (rt90arr) { return arrayToLatLngObject(convert("RT90", "WGS84", rt90arr), true); },
                movePointsByCoord: movePointsByCoord
            },
            arrToLatLngObj: arrayToLatLngObject,
            convert: convert,
            haversineDistance: haversineDistance,
            MVCArrayToArrayOfArrays: MVCArrayToArrayOfArrays,
            MVCArrayToObjArray: MVCArrayToObjArray
        };
        return _this;
    }
    WrappedMapBase.prototype.componentWillMount = function () {
        this.script_cache = ScriptCache_1.default({
            google: 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,places,drawing&key=' + this.props.api_key
        });
    };
    WrappedMapBase.prototype.componentWillUnmount = function () {
        if (this.map && this.initialized) {
            window.google.maps.event.clearInstanceListeners(this.map);
        }
    };
    WrappedMapBase.prototype.componentDidMount = function () {
        var _this = this;
        var refs = this.refs;
        if (this.props.id) {
            if (window.hasOwnProperty("allbin_gmaps")) {
                window.wrapped_gmaps[this.props.id] = this;
            }
        }
        this.script_cache.google.onLoad(function (err, tag) {
            function CanvasProjectionOverlay() { }
            CanvasProjectionOverlay.prototype = new window.google.maps.OverlayView();
            CanvasProjectionOverlay.prototype.constructor = CanvasProjectionOverlay;
            CanvasProjectionOverlay.prototype.onAdd = function () { };
            CanvasProjectionOverlay.prototype.draw = function () { };
            CanvasProjectionOverlay.prototype.onRemove = function () { };
            var mapRef = refs.map;
            _this.html_element = ReactDOM.findDOMNode(mapRef);
            var center = _this.props.center || _this.props.defaultCenter;
            if (!center) {
                throw new Error("Could not create map: Requires either 'center' or 'defaultCenter' prop.");
            }
            var zoom = (typeof _this.props.zoom !== "undefined") ? _this.props.zoom : (typeof _this.props.defaultZoom !== "undefined") ? _this.props.defaultZoom : null;
            if (!zoom) {
                throw new Error("Could not create map: Requires either 'zoom' or 'defaultZoom' prop.");
            }
            if (!_this.props.api_key) {
                throw new Error("Could not create map: Requires 'api_key' prop.");
            }
            var defaults = _this.props.defaultOptions || {};
            var mapConfig = Object.assign({}, defaults, {
                center: new window.google.maps.LatLng(center.lat, center.lng),
                zoom: zoom,
                gestureHandling: 'greedy',
                styles: _this.props.styles || {}
            });
            var maps = window.google.maps;
            _this.map = new maps.Map(_this.html_element, mapConfig);
            _this.services = {
                geocoderService: new window.google.maps.Geocoder(),
                directionsService: new window.google.maps.DirectionsService(),
            };
            if (window.google.maps.drawing) {
                _this.services.drawing = window.google.maps.drawing;
                _this.services.drawingManager = new window.google.maps.drawing.DrawingManager({
                    drawingMode: null,
                    drawingControl: false,
                    drawingControlOptions: {
                        drawingModes: []
                    }
                });
                _this.services.drawingManager.setMap(_this.map);
            }
            _this.overlay = new CanvasProjectionOverlay();
            _this.overlay.setMap(_this.map);
            _this.setupMapEvents(_this.map);
            window.google.maps.event.addListenerOnce(_this.map, 'idle', function () { _this.doAfterInit(); });
        });
    };
    // componentDidUpdate(prev_props) {
    //     let new_map_opts = {};
    //     if (this.props.styles !== prev_props.styles) {
    //         //Styles have updated.
    //         Object.assign(new_map_opts, { styles: this.props.styles });
    //     }
    //     if (this.props.center !== prev_props.center) {
    //         this.map.setCenter(this.props.center);
    //     }
    //     if (this.props.zoom !== prev_props.zoom) {
    //         this.map.setCenter(this.props.zoom);
    //     }
    //     if (Object.keys(new_map_opts).length > 0) {
    //         this.map.setOptions(Object.assign(
    //             {},
    //             prev_props.styles,
    //             new_map_opts
    //         ));
    //     }
    // }
    WrappedMapBase.prototype.doAfterInit = function () {
        this.initialized = true;
        this.do_after_init.forEach(function (cb) {
            cb();
        });
        if (this.props.initializedCB) {
            //Tell parent we are initialized if the parent has asked for it.
            this.props.initializedCB();
        }
    };
    WrappedMapBase.prototype.setCenter = function (latLng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.initialized) {
                _this.do_after_init.push(function () {
                    _this.setCenter(latLng).then(function (res) {
                        resolve(res);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
                return;
            }
            _this.map.setCenter(latLng);
            resolve();
            return;
        });
    };
    WrappedMapBase.prototype.fitToBoundsArray = function (arr_of_arrays) {
        return fitToBoundsOfArray(this, arr_of_arrays);
    };
    WrappedMapBase.prototype.fitToBoundsObjectArray = function (arr_of_objects) {
        return fitToBoundsOfObjectArray(this, arr_of_objects);
    };
    WrappedMapBase.prototype.toPixel = function (lat_lng_input) {
        var node_rect = this.html_element.getBoundingClientRect();
        var lat_lng;
        if (lat_lng_input instanceof google.maps.LatLng) {
            lat_lng = lat_lng_input;
        }
        else {
            lat_lng = new window.google.maps.LatLng(lat_lng_input);
        }
        var pixel_obj = this.overlay.getProjection().fromLatLngToContainerPixel(lat_lng);
        return [pixel_obj.x + node_rect.left, pixel_obj.y + node_rect.top];
    };
    WrappedMapBase.prototype.setZoom = function (zoom_level) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.initialized) {
                _this.do_after_init.push(function () {
                    _this.setZoom(zoom_level).then(function (res) {
                        resolve(res);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
                return;
            }
            _this.map.setZoom(zoom_level);
            resolve();
            return;
        });
    };
    WrappedMapBase.prototype.setPolyline = function (id, options, hover_options, highlight_options) {
        if (hover_options === void 0) { hover_options = null; }
        if (highlight_options === void 0) { highlight_options = null; }
        return setMapObject(this, "polyline", id, options, hover_options, highlight_options);
    };
    WrappedMapBase.prototype.unsetPolyline = function (id) {
        return unsetMapObject(this, "polyline", id);
    };
    WrappedMapBase.prototype.clearPolylines = function () {
        var _this = this;
        var promise_arr = [];
        Object.keys(this.map_objects.polyline).forEach(function (id) {
            promise_arr.push(unsetMapObject(_this, "polyline", id));
        });
        return Promise.all(promise_arr);
    };
    WrappedMapBase.prototype.setPolygon = function (id, options, hover_options, highlight_options) {
        if (hover_options === void 0) { hover_options = null; }
        if (highlight_options === void 0) { highlight_options = null; }
        return setMapObject(this, "polygon", id, options, hover_options, highlight_options);
    };
    WrappedMapBase.prototype.unsetPolygon = function (id) {
        return unsetMapObject(this, "polygon", id);
    };
    WrappedMapBase.prototype.clearPolygons = function () {
        var _this = this;
        var promise_arr = [];
        Object.keys(this.map_objects.polygon).forEach(function (id) {
            promise_arr.push(unsetMapObject(_this, "polygon", id));
        });
        return Promise.all(promise_arr);
    };
    WrappedMapBase.prototype.setDirections = function (id, options, hover_options) {
        if (hover_options === void 0) { hover_options = null; }
        return setMapObject(this, "directions", id, options, hover_options);
    };
    WrappedMapBase.prototype.unsetDirections = function (id) {
        return unsetMapObject(this, "directions", id);
    };
    WrappedMapBase.prototype.clearDirections = function () {
        var _this = this;
        var promise_arr = [];
        Object.keys(this.map_objects.directions).forEach(function (id) {
            promise_arr.push(unsetMapObject(_this, "directions", id));
        });
        return Promise.all(promise_arr);
    };
    WrappedMapBase.prototype.setMarker = function (id, options, hover_options, highlight_options) {
        if (hover_options === void 0) { hover_options = null; }
        if (highlight_options === void 0) { highlight_options = null; }
        return setMapObject(this, "marker", id, options, hover_options, highlight_options);
    };
    WrappedMapBase.prototype.unsetMarker = function (id) {
        return unsetMapObject(this, "marker", id);
    };
    WrappedMapBase.prototype.clearMarkers = function () {
        var _this = this;
        var promise_arr = [];
        Object.keys(this.map_objects.marker).forEach(function (id) {
            promise_arr.push(unsetMapObject(_this, "marker", id));
        });
        return Promise.all(promise_arr);
    };
    WrappedMapBase.prototype.registerDragEndCB = function (cb) {
        //Is actually triggered by Idle, not DragEnd!
        this.do_on_drag_end.push(cb);
    };
    WrappedMapBase.prototype.unregisterDragEndCB = function (cb) {
        var index = this.do_on_drag_end.indexOf(cb);
        if (index > -1) {
            this.do_on_drag_end.splice(index, 1);
        }
    };
    WrappedMapBase.prototype.registerDragStartCB = function (cb) {
        this.do_on_drag_end.push(cb);
    };
    WrappedMapBase.prototype.unregisterDragStartCB = function (cb) {
        var index = this.do_on_drag_start.indexOf(cb);
        if (index > -1) {
            this.do_on_drag_start.splice(index, 1);
        }
    };
    WrappedMapBase.prototype.setupMapEvents = function (map) {
        var _this = this;
        map.addListener('center_changed', function () {
            if (_this.props.onCenterChanged) {
                _this.props.onCenterChanged();
            }
        });
        map.addListener('bounds_changed', function () {
            if (_this.props.onBoundsChanged) {
                _this.props.onBoundsChanged();
            }
        });
        map.addListener('click', function (mouse_event) {
            if (_this.cutting.enabled) {
                _this.cuttingClick(mouse_event);
            }
            if (_this.props.onClick && !_this.cutting.enabled) {
                _this.props.onClick(mouse_event);
            }
        });
        map.addListener('dblclick', function (mouse_event) {
            if (_this.props.onDoubleClick && !_this.cutting.enabled) {
                _this.props.onDoubleClick(mouse_event);
            }
        });
        map.addListener('drag', function () {
            if (_this.props.onDrag && !_this.cutting.enabled) {
                _this.props.onDrag();
            }
        });
        map.addListener('dragend', function () {
            if (_this.props.onDragEnd && !_this.cutting.enabled) {
                _this.props.onDragEnd();
            }
        });
        map.addListener('dragstart', function () {
            _this.do_on_drag_start.forEach(function (cb) {
                if (!_this.cutting.enabled) {
                    cb();
                }
            });
            if (_this.props.onDragStart && !_this.cutting.enabled) {
                _this.props.onDragStart();
            }
        });
        map.addListener('heading_changed', function () {
            if (_this.props.onHeadingChanged) {
                _this.props.onHeadingChanged();
            }
        });
        map.addListener('idle', function () {
            _this.do_on_drag_end.forEach(function (cb) {
                if (!_this.cutting.enabled) {
                    cb();
                }
            });
            if (_this.props.onIdle && !_this.cutting.enabled) {
                _this.props.onIdle();
            }
        });
        map.addListener('maptypeid_changed', function () {
            if (_this.props.onMapTypeIdChanged) {
                _this.props.onMapTypeIdChanged();
            }
        });
        map.addListener('mousemove', function (mouse_event) {
            if (_this.cutting.enabled) {
                _this.cuttingPositionUpdate(mouse_event);
            }
            if (_this.props.onMouseMove) {
                _this.props.onMouseMove(mouse_event);
            }
        });
        map.addListener('mouseout', function (mouse_event) {
            if (_this.props.onMouseOut) {
                _this.props.onMouseOut(mouse_event);
            }
        });
        map.addListener('mouseover', function (mouse_event) {
            if (_this.props.onMouseOver) {
                _this.props.onMouseOver(mouse_event);
            }
        });
        map.addListener('projection_changed', function () {
            if (_this.props.onProjectionChanged) {
                _this.props.onProjectionChanged();
            }
        });
        map.addListener('reize', function () {
            if (_this.props.onResize) {
                _this.props.onResize();
            }
        });
        map.addListener('rightclick', function (mouse_event) {
            if (_this.props.onRightClick && !_this.cutting.enabled) {
                _this.props.onRightClick(mouse_event);
            }
        });
        map.addListener('tilesloaded', function () {
            if (_this.props.onTilesLoaded) {
                _this.props.onTilesLoaded();
            }
        });
        map.addListener('tilt_changed', function () {
            if (_this.props.onTiltChanged) {
                _this.props.onTiltChanged();
            }
        });
        map.addListener('zoom_changed', function () {
            if (_this.props.onZoomChanged) {
                _this.props.onZoomChanged();
            }
        });
    };
    WrappedMapBase.prototype.setDrawingMode = function (type, opts, cb) {
        var _this = this;
        if (cb === void 0) { cb = null; }
        var mode = null;
        if (!this.services.drawing) {
            console.error("MAP: Drawing library not available! Add it to google maps api request url.");
            return;
        }
        if (this.services.drawing.OverlayType.hasOwnProperty(type.toUpperCase())) {
            mode = this.services.drawing.OverlayType[type.toUpperCase()];
        }
        else {
            throw new Error("MAP: Invalid drawing mode type: " + type);
        }
        var drawing_opts = Object.assign({}, opts, { drawingMode: mode });
        this.services.drawingManager.setOptions(drawing_opts);
        console.log("MAP: Drawing mode started for: ", type, ".");
        if (this.drawing_completed_listener) {
            this.drawing_completed_listener.remove();
        }
        this.drawing_completed_listener = window.google.maps.event.addListenerOnce(this.services.drawingManager, 'overlaycomplete', function (e) {
            e.overlay.setMap(null);
            drawing_opts.drawingMode = null;
            _this.services.drawingManager.setOptions(drawing_opts);
            if (!cb) {
                return;
            }
            if (type === "polyline" || type === "polygon") {
                var path = MVCArrayToArrayOfArrays(e.overlay.getPath());
                cb(path, e.overlay);
            }
            else if (type === "marker") {
                var pos = e.overlay.getPosition();
                cb([pos.lat(), pos.lng()], e.overlay);
            }
            else {
                cb(null, e.overlay);
            }
        });
    };
    WrappedMapBase.prototype.completeDrawingMode = function () {
        if (this.services.drawing) {
            this.services.drawingManager.setOptions({ drawingMode: null });
        }
        if (this.drawing_completed_listener) {
            this.drawing_completed_listener.remove();
            this.drawing_completed_listener = null;
        }
    };
    WrappedMapBase.prototype.cancelDrawingMode = function () {
        if (this.drawing_completed_listener) {
            this.drawing_completed_listener.remove();
            this.drawing_completed_listener = null;
        }
        if (!this.services.drawing) {
            return;
        }
        this.services.drawingManager.setOptions({ drawingMode: null });
    };
    WrappedMapBase.prototype.setCuttingMode = function (polyline_id, cb) {
        if (cb === void 0) { cb = null; }
        if (this.map_objects.polyline.hasOwnProperty(polyline_id) === false) {
            console.error("MAP: Cannot set cutting mode, provided object id not on map: ", polyline_id);
            return;
        }
        if (!cb) {
            console.error("MAP: Cannot setCuttingMode without supplying completed callback.");
            return;
        }
        this.cancelDrawingMode();
        var polyline = this.map_objects.polyline[polyline_id];
        var opts = {
            clickable: false,
            visible: true,
            draggable: false,
            editable: false
        };
        polyline.gmaps_obj.setOptions(opts);
        this.cutting = {
            enabled: true,
            id: polyline_id,
            indexes: [],
            arr: polyline.options.path
        };
        if (!this.cutting_objects.hasOwnProperty("hover_scissors")) {
            var opts_1 = {
                position: this.props.defaultCenter,
                icon: {
                    url: ScissorHoverIcon
                },
                zIndex: Z_INDEX_SCISSORS_HOVER,
                visible: false,
                clickable: false,
                editable: false,
                draggable: false
            };
            var hover_scissors = {
                gmaps_obj: new window.google.maps.Marker(opts_1),
                options: opts_1
            };
            hover_scissors.gmaps_obj.setMap(this.map);
            this.cutting_objects.hover_scissors = hover_scissors;
        }
        console.log("MAP: Cutting mode started.");
        this.cutting_completed_listener = function (value) {
            cb(value);
        };
    };
    WrappedMapBase.prototype.cuttingPositionUpdate = function (mouse_event) {
        if (!this.cutting.enabled) {
            //If we are not in cutting mode ignore this function call.
            return;
        }
        var polyline = this.map_objects.polyline[this.cutting.id];
        var mouse_coord = { lat: mouse_event.latLng.lat(), lng: mouse_event.latLng.lng() };
        var closest_index = 0;
        var closest_dist = 9999999;
        //Find nearest index and move scissors_hover marker.
        polyline.options.path.forEach(function (point, i) {
            var dist = haversineDistance(mouse_coord, point);
            if (dist < closest_dist) {
                closest_index = i;
                closest_dist = dist;
            }
        });
        if (closest_dist < CUTTING_SNAP_DISTANCE) {
            this.cutting_objects.hover_scissors.gmaps_obj.setOptions({
                position: polyline.options.path[closest_index],
                visible: true
            });
        }
        else {
            this.cutting_objects.hover_scissors.gmaps_obj.setOptions({
                visible: false
            });
        }
    };
    WrappedMapBase.prototype.cuttingClick = function (mouse_event) {
        var polyline = this.map_objects.polyline[this.cutting.id];
        var mouse_coord = { lat: mouse_event.latLng.lat(), lng: mouse_event.latLng.lng() };
        var closest_index = 0;
        var closest_dist = 9999999;
        polyline.options.path.forEach(function (point, i) {
            var dist = haversineDistance(mouse_coord, point);
            if (dist < closest_dist) {
                closest_index = i;
                closest_dist = dist;
            }
        });
        if (closest_dist > CUTTING_SNAP_DISTANCE) {
            //Pointer is too far away from any point, ignore.
            return;
        }
        if (closest_index === 0 || closest_index === polyline.options.path.length - 1) {
            //We are never interested in first or last point.
            return;
        }
        var already_selected_position = this.cutting.indexes.findIndex(function (value) { return closest_index === value; });
        if (already_selected_position > -1) {
            //This index has already been selected for cutting, remove it.
            this.cutting.indexes.splice(already_selected_position, 1);
            if (this.cutting_objects.hasOwnProperty("index_" + closest_index)) {
                //We have drawn a marker for this cut, remove it.
                this.cutting_objects["index_" + closest_index].gmaps_obj.setMap(null);
                delete this.cutting_objects["index_" + closest_index];
            }
        }
        else {
            this.cutting.indexes.push(closest_index);
            var opts = {
                position: polyline.options.path[closest_index],
                icon: {
                    url: ScissorIcon
                },
                zIndex: Z_INDEX_SCISSORS,
                visible: true,
                clickable: false,
                editable: false,
                draggable: false
            };
            var cut_marker = {
                gmaps_obj: new window.google.maps.Marker(opts),
                options: opts
            };
            cut_marker.gmaps_obj.setMap(this.map);
            this.cutting_objects["index_" + closest_index] = cut_marker;
        }
    };
    WrappedMapBase.prototype.completeCuttingMode = function () {
        var _this = this;
        var indexes = this.cutting.indexes;
        var polyline = this.map_objects.polyline[this.cutting.id];
        this.cutting = {
            enabled: false,
            id: null,
            indexes: null
        };
        Object.keys(this.cutting_objects).forEach(function (marker_id) {
            //Remove all cutting related markers.
            _this.cutting_objects[marker_id].gmaps_obj.setMap(null);
            delete _this.cutting_objects[marker_id];
        });
        var opts = {
            clickable: false,
            visible: false,
            editable: false,
            draggable: false,
        };
        if (polyline.hovered) {
            opts = Object.assign(opts, polyline.options, polyline.hover_options);
        }
        else {
            opts = Object.assign(opts, polyline.options);
        }
        //Reset appearance of polyline to what was before cutting mode.
        polyline.gmaps_obj.setOptions(opts);
        var path = polyline.options.path;
        indexes.sort();
        //Add last index so that the remaining points form a segment as well.
        indexes.push(path.length - 1);
        var resulting_segments = [];
        var prev_index = 0;
        indexes.forEach(function (index) {
            var segment = path.slice(prev_index, index);
            //Copy last point as well.
            segment.push(path[index]);
            resulting_segments.push(segment);
            prev_index = index;
        });
        this.cutting_completed_listener(resulting_segments);
    };
    WrappedMapBase.prototype.render = function () {
        return (React.createElement("div", { style: { height: "100%" } },
            React.createElement("div", { ref: "map", style: { position: "absolute", top: '0', left: '0', right: '0', bottom: '0' } })));
    };
    return WrappedMapBase;
}(React.Component));
exports.default = WrappedMapBase;
/////////////////////////////////
//INTERNAL MAP HELPER FUNCTIONS
//
//
function fitToBoundsOfArray(map_ref, arr_of_arrays) {
    //Takes [[x, y], ...] array.
    return new Promise(function (resolve, reject) {
        if (Array.isArray(arr_of_arrays) === false) {
            reject("Input not valid array.");
        }
        else if (arr_of_arrays.length < 1) {
            reject("Array needs to countain at least one element.");
        }
        if (!map_ref.initialized) {
            map_ref.do_after_init.push(function () {
                fitToBoundsOfArray(map_ref, arr_of_arrays).then(function (res) {
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            });
            return;
        }
        var lat_lng_literal = {
            east: -99999999,
            west: 99999999,
            north: 99999999,
            south: -99999999
        };
        arr_of_arrays.forEach(function (point) {
            lat_lng_literal.west = (point[0] < lat_lng_literal.west) ? point[0] : lat_lng_literal.west;
            lat_lng_literal.east = (point[0] > lat_lng_literal.east) ? point[0] : lat_lng_literal.east;
            lat_lng_literal.north = (point[1] < lat_lng_literal.north) ? point[1] : lat_lng_literal.north;
            lat_lng_literal.south = (point[1] > lat_lng_literal.south) ? point[1] : lat_lng_literal.south;
        });
        map_ref.map.fitBounds(lat_lng_literal);
        resolve();
    });
}
function fitToBoundsOfObjectArray(map_ref, arr_of_objects) {
    //Takes [{ lat: ?, lng: ? }, ...] array.
    return new Promise(function (resolve, reject) {
        if (Array.isArray(arr_of_objects) === false) {
            reject("Input not valid array.");
        }
        else if (arr_of_objects.length < 1) {
            reject("Array needs to countain at least one element.");
        }
        if (!map_ref.initialized) {
            map_ref.do_after_init.push(function () {
                fitToBoundsOfObjectArray(map_ref, arr_of_objects).then(function (res) {
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            });
            return;
        }
        var lat_lng_literal = {
            east: -99999999,
            west: 99999999,
            north: 99999999,
            south: -99999999
        };
        arr_of_objects.forEach(function (point) {
            lat_lng_literal.west = (point.lng < lat_lng_literal.west) ? point.lng : lat_lng_literal.west;
            lat_lng_literal.east = (point.lng > lat_lng_literal.east) ? point.lng : lat_lng_literal.east;
            lat_lng_literal.north = (point.lat < lat_lng_literal.north) ? point.lat : lat_lng_literal.north;
            lat_lng_literal.south = (point.lat > lat_lng_literal.south) ? point.lat : lat_lng_literal.south;
        });
        map_ref.map.fitBounds(lat_lng_literal);
        resolve();
    });
}
function setMapObject(map_ref, type, id, options, hover_options, highlight_options) {
    if (hover_options === void 0) { hover_options = null; }
    if (highlight_options === void 0) { highlight_options = null; }
    return new Promise(function (resolve, reject) {
        if (!map_ref.initialized) {
            map_ref.do_after_init.push(function () {
                setMapObject(map_ref, type, id, options, hover_options, highlight_options).then(function (res) {
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            });
            return;
        }
        if (map_ref.map_objects[type].hasOwnProperty(id)) {
            //This ID has already been drawn.
            var map_obj_1 = map_ref.map_objects[type][id];
            map_obj_1.highlight_options = highlight_options;
            var opts = void 0;
            if (map_obj_1.hovered && hover_options) {
                opts = Object.assign({}, map_obj_1.options, options, hover_options);
            }
            else {
                opts = Object.assign({}, map_obj_1.options, options);
            }
            map_obj_1.gmaps_obj.setOptions(opts);
            map_obj_1.options = opts;
            map_obj_1.hover_options = hover_options;
            if (map_obj_1.highlighted && highlight_options) {
                //The highlight has been called on this object AND we do have highlight options.
                map_obj_1.highlight();
            }
            resolve(map_obj_1);
            return;
        }
        var map_obj;
        var events = [];
        var path_events = [];
        switch (type) {
            case "directions": {
                var opts = Object.assign({}, DEFAULT_DIRECTIONS_OPTIONS, options);
                hover_options = null; //Directions cannot have hover event.
                highlight_options = null; //Directions cannot have highlight.
                map_obj = {
                    gmaps_obj: new window.google.maps.DirectionsRenderer(opts),
                    options: opts
                };
                break;
            }
            case "marker": {
                var opts = Object.assign({}, DEFAULT_MARKER_OPTIONS, options);
                map_obj = {
                    gmaps_obj: new window.google.maps.Marker(opts),
                    options: opts
                };
                events = ["click", "mouseover", "mouseout", "mousedown", "mouseup", "dragstart", "drag", "dragend", "dblclick", "rightclick"];
                break;
            }
            case "polygon": {
                var opts = Object.assign({}, DEFAULT_POLYGON_OPTIONS, options);
                map_obj = {
                    gmaps_obj: new window.google.maps.Polygon(opts),
                    options: opts
                };
                events = ["click", "dblclick", "dragstart", "drag", "dragend", "mouseover", "mouseout", "mousedown", "mouseup", "mousemove", "rightclick"];
                path_events = ["set_at", "remove_at", "insert_at"];
                break;
            }
            case "polyline": {
                var opts = Object.assign({}, DEFAULT_POLYLINE_OPTIONS, options);
                map_obj = {
                    gmaps_obj: new window.google.maps.Polyline(opts),
                    options: opts
                };
                events = ["click", "dblclick", "dragstart", "drag", "dragend", "mouseover", "mouseout", "mousedown", "mouseup", "mousemove", "rightclick"];
                path_events = ["set_at", "remove_at", "insert_at"];
                break;
            }
            default: {
                reject(new Error("Invalid map object type."));
            }
        }
        map_obj.hover_options = hover_options;
        map_obj.hovered = false;
        map_obj.highlight_options = highlight_options;
        map_obj.highlighted = false;
        map_obj._cbs = {};
        events.forEach(function (event_type) {
            map_obj.gmaps_obj.addListener(event_type, function (e) { return mapObjectEventCB(map_ref, map_obj, event_type, e); });
        });
        path_events.forEach(function (event_type) {
            map_obj.gmaps_obj.getPath().addListener(event_type, function (e) { return mapObjectEventCB(map_ref, map_obj, event_type, e); });
        });
        map_obj.registerEventCB = function (event_type, cb) {
            if (type === "directions") {
                console.error("Directions renderer does not support events.");
            }
            map_obj._cbs[event_type] = cb;
        };
        map_obj.unregisterEventCB = function (event_type) {
            if (map_obj._cbs.hasOwnProperty(event_type)) {
                delete map_obj._cbs[event_type];
            }
        };
        map_obj.hover = function () {
            if (!map_obj.hover_options) {
                return;
            }
            var opts = Object.assign({}, map_obj.options, map_obj.hover_options);
            var whitelisted_opts = {
                strokeColor: opts.strokeColor,
                strokeWidth: opts.strokeWidth,
                fillColor: opts.fillColor,
                fillOpacity: opts.fillOpacity
            };
            map_obj.gmaps_obj.setOptions(whitelisted_opts);
            map_obj.hovered = true;
        };
        map_obj.unhover = function () {
            var opts = Object.assign({}, map_obj.options);
            var whitelisted_opts = {
                strokeColor: opts.strokeColor,
                strokeWidth: opts.strokeWidth,
                fillColor: opts.fillColor,
                fillOpacity: opts.fillOpacity
            };
            map_obj.gmaps_obj.setOptions(whitelisted_opts);
            map_obj.hovered = false;
        };
        map_obj.remove = function () { return unsetMapObject(map_ref, type, id); };
        map_obj.update = function (new_options) { return setMapObject(map_ref, type, id, new_options, hover_options); };
        map_obj.update_hover = function (new_hover_options) { return setMapObject(map_ref, type, id, options, new_hover_options); };
        map_obj.hide = function () {
            map_obj.gmaps_obj.setOptions(Object.assign({}, map_obj.options, { visible: false }));
            if (map_ref.highlight_objects[type].hasOwnProperty(id)) {
                map_ref.highlight_objects[type][id].gmaps_obj.setOptions({ visible: false });
            }
        };
        map_obj.show = function () {
            map_obj.gmaps_obj.setOptions(Object.assign({}, map_obj.options, { visible: true }));
            if (map_obj.highlighted) {
                map_obj.highlight();
            }
        };
        map_obj.highlight = function () {
            if (!map_obj.highlight_options) {
                if (type === "directions") {
                    console.log("MAP: Map directions do not support highlighting. Use custom functionality.");
                    return;
                }
                console.log("MAP: Cannot highlight, no highlight_options specified.");
                return;
            }
            var high_opts = Object.assign({}, map_obj.options, options, map_obj.highlight_options, { visible: true, clickable: false });
            if (map_ref.highlight_objects[type].hasOwnProperty(id)) {
                var high_obj = map_ref.highlight_objects[type][id];
                high_obj.gmaps_obj.setOptions(high_opts);
            }
            else {
                createHighlightObject(map_ref, type, id, high_opts);
            }
            var opts;
            if (map_obj.hovered && map_obj.hover_options) {
                opts = Object.assign({}, map_obj.options, options, map_obj.hover_options);
            }
            else {
                opts = Object.assign({}, map_obj.options, options);
            }
            map_obj.gmaps_obj.setOptions(Object.assign({}, opts, { zIndex: Z_INDEX_HIGHLIGHTED }));
        };
        map_obj.unhighlight = function () {
            if (map_ref.highlight_objects[type].hasOwnProperty(id)) {
                var high_obj = map_ref.highlight_objects[type][id];
                var high_opts = Object.assign({}, map_obj.options, options, map_obj.highlight_options, { visible: false });
                high_obj.gmaps_obj.setOptions(high_opts);
            }
            var opts;
            if (map_obj.hovered && map_obj.hover_options) {
                opts = Object.assign({}, map_obj.options, options, map_obj.hover_options);
            }
            else {
                opts = Object.assign({}, map_obj.options, options);
            }
            map_obj.gmaps_obj.setOptions(Object.assign({}, opts));
        };
        map_obj.gmaps_obj.setMap(map_ref.map);
        map_ref.map_objects[type][id] = map_obj;
        resolve(map_ref.map_objects[type][id]);
        return;
    });
}
function unsetMapObject(map_ref, type, id) {
    return new Promise(function (resolve, reject) {
        if (!map_ref.initialized) {
            map_ref.do_after_init.push(function () {
                map_ref.unsetMapObject(id).then(function (res) {
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            });
            return;
        }
        if (map_ref.map_objcts[type].hasOwnProperty(id)) {
            //This ID has been drawn.
            if (map_ref.cutting.id !== id) {
                //This object is currently being cut, it cannot be deleted.
                reject(new Error("MAP: Object is currently in cuttingMode; it cannot be removed!"));
                return;
            }
            map_ref.map_objcts[type][id].gmaps_obj.setMap(null);
            delete map_ref.map_objcts[type][id];
            if (map_ref.highlight_objects[type].hasOwnProperty(id)) {
                //This object also has a highlight object attached to it.
                map_ref.highlight_objects[type][id].gmaps_obj.setMap(null);
                delete map_ref.highlight_objects[type][id];
            }
            resolve(true);
            return;
        }
        reject(new Error("MAP: MapObject does not exist."));
    });
}
function mapObjectEventCB(map_ref, map_obj, event_type, e) {
    if (map_ref.cutting.enabled) {
        //When the map is in cutting mode no object event callbacks are allowed.
        return true;
    }
    if (event_type === "mouseover") {
        map_obj.hover();
    }
    if (event_type === "mouseout") {
        map_obj.unhover();
    }
    if (map_obj._cbs.hasOwnProperty(event_type) && map_obj._cbs[event_type]) {
        map_obj._cbs[event_type](e);
    }
    return true;
}
function createHighlightObject(map_ref, type, id, opts) {
    opts.zIndex = Z_INDEX_HIGHLIGHTS;
    opts.clickable = false;
    var high_obj;
    switch (type) {
        case "marker": {
            high_obj = {
                gmaps_obj: new window.google.maps.Marker(opts),
                options: opts
            };
            break;
        }
        case "polygon": {
            high_obj = {
                gmaps_obj: new window.google.maps.Polygon(opts),
                options: opts
            };
            break;
        }
        case "polyline": {
            high_obj = {
                gmaps_obj: new window.google.maps.Polyline(opts),
                options: opts
            };
            break;
        }
        default: {
            throw new Error("Invalid map object type for highlighting.");
        }
    }
    high_obj.gmaps_obj.setMap(map_ref.map);
    map_ref.highlight_objects[type][id] = high_obj;
}
////////////EXPORTED HELPER FUNCTIONS
//Check Map.helpers for usage.
function convert(fromProj, toProj, points) {
    var newPointArr = [];
    if (typeof points[0] === "object") {
        points.forEach(function (point) {
            newPointArr.push(proj4_1.default(fromProj, toProj, point));
        });
    }
    else {
        newPointArr = proj4_1.default(fromProj, toProj, points);
    }
    return newPointArr;
}
function arrayToLatLngObject(arr, invert) {
    if (invert === void 0) { invert = false; }
    if (invert) {
        return arr.map(function (point) {
            return { lat: point[1], lng: point[0] };
        });
    }
    return arr.map(function (point) {
        return { lat: point[0], lng: point[1] };
    });
}
function makePointsAroundCircleRT90(p, r, numberOfPoints) {
    //Returns numberOfPoints around circle at p with r radius.
    if (numberOfPoints === void 0) { numberOfPoints = 12; }
    var points = [];
    var i;
    for (i = 0; i < numberOfPoints; i += 1) {
        points.push([
            p[0] + r * Math.cos(2 * Math.PI * i / numberOfPoints),
            p[1] + r * Math.sin(2 * Math.PI * i / numberOfPoints)
        ]);
    }
    return points;
}
function makeRectRT90(p1, p2, chamfer) {
    if (chamfer === void 0) { chamfer = { r: 0, points: 0 }; }
    //TODO: Chamfer.
    //p1 and p2 should be opposite corners of the rectangle.
    var points = [];
    points.push([p1[0], p1[1]], [p2[0], p1[1]], [p2[0], p2[1]], [p1[0], p2[1]]);
    if (chamfer.r > 0) {
        var c_points = [];
        //TODO: Add code here.
        return c_points;
    }
    return points;
}
function movePointsByCoord(points_arr, coord) {
    //Adds value of Coord to all points in array.
    return points_arr.map(function (point) {
        return [point[0] + coord[0], point[1] + coord[1]];
    });
}
function squared(x) { return x * x; }
function toRad(x) { return x * Math.PI / 180; }
function haversineDistance(a, b) {
    var aLat = a.lat;
    var bLat = b.lat;
    var aLng = a.lng;
    var bLng = b.lng;
    var dLat = toRad(bLat - aLat);
    var dLon = toRad(bLng - aLng);
    var f = squared(Math.sin(dLat / 2.0)) + Math.cos(toRad(aLat)) * Math.cos(toRad(bLat)) * squared(Math.sin(dLon / 2.0));
    var c = 2 * Math.atan2(Math.sqrt(f), Math.sqrt(1 - f));
    return EARTH_RADIUS * c;
}
function MVCArrayToObjArray(MVCArr) {
    return MVCArr.getArray().map(function (gmapsLatLng) {
        return {
            lat: gmapsLatLng.lat(),
            lng: gmapsLatLng.lng()
        };
    });
}
function MVCArrayToArrayOfArrays(MVCArr) {
    return MVCArr.getArray().map(function (gmapsLatLng) {
        return [gmapsLatLng.lat(), gmapsLatLng.lng()];
    });
}
function fitBoundsWithPadding(gMap, bounds, paddingXY) {
    // taken from: https://stackoverflow.com/questions/10339365/google-maps-api-3-fitbounds-padding-ensure-markers-are-not-obscured-by-overlai
    var projection = gMap.getProjection();
    if (projection) {
        if (!paddingXY) {
            paddingXY = { x: 0, y: 0 };
        }
        var paddings = {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
        };
        if (paddingXY.left) {
            paddings.left = paddingXY.left;
        }
        else if (paddingXY.x) {
            paddings.left = paddingXY.x;
            paddings.right = paddingXY.x;
        }
        if (paddingXY.right) {
            paddings.right = paddingXY.right;
        }
        if (paddingXY.top) {
            paddings.top = paddingXY.top;
        }
        else if (paddingXY.y) {
            paddings.top = paddingXY.y;
            paddings.bottom = paddingXY.y;
        }
        if (paddingXY.bottom) {
            paddings.bottom = paddingXY.bottom;
        }
        // copying the bounds object, since we will extend it
        bounds = new window.google.maps.LatLngBounds(bounds.getSouthWest(), bounds.getNorthEast());
        gMap.fitBounds(bounds);
        // SW
        var point1 = projection.fromLatLngToPoint(bounds.getSouthWest());
        var point2 = new window.google.maps.Point(((typeof (paddings.left) === 'number' ? paddings.left : 0) / Math.pow(2, gMap.getZoom() - 1)) || 0, ((typeof (paddings.bottom) === 'number' ? paddings.bottom : 0) / Math.pow(2, gMap.getZoom() - 1)) || 0);
        var newPoint = projection.fromPointToLatLng(new window.google.maps.Point(point1.x - point2.x, point1.y + point2.y));
        bounds.extend(newPoint);
        // NE
        point1 = projection.fromLatLngToPoint(bounds.getNorthEast());
        point2 = new window.google.maps.Point(((typeof (paddings.right) === 'number' ? paddings.right : 0) / Math.pow(2, gMap.getZoom() - 1)) || 0, ((typeof (paddings.top) === 'number' ? paddings.top : 0) / Math.pow(2, gMap.getZoom() - 1)) || 0);
        newPoint = projection.fromPointToLatLng(new window.google.maps.Point(point1.x + point2.x, point1.y - point2.y));
        bounds.extend(newPoint);
        gMap.fitBounds(bounds);
    }
}
exports.fitBoundsWithPadding = fitBoundsWithPadding;

//# sourceMappingURL=index.js.map
