/// <reference types="googlemaps" />
import * as React from 'react';
declare global {
    interface Window {
        google: any;
        wrapped_gmaps: any;
    }
}
interface LooseObject {
    [key: string]: any;
}
export interface LatLngLiteral {
    lat: number;
    lng: number;
}
export interface MapBaseProps {
    initializedCB?: () => void;
    api_key: string;
    id?: string;
    center?: google.maps.LatLngLiteral;
    zoom?: number;
    defaultCenter?: google.maps.LatLngLiteral;
    defaultZoom?: number;
    defaultOptions?: object;
    onCenterChanged?: () => void;
    onBoundsChanged?: () => void;
    onClick?: (any: any) => void;
    onDoubleClick?: (any: any) => void;
    onDrag?: () => void;
    onDragEnd?: () => void;
    onDragStart?: () => void;
    onHeadingChanged?: () => void;
    onIdle?: () => void;
    onMapTypeIdChanged?: () => void;
    onMouseMove?: (any: any) => void;
    onMouseOut?: (any: any) => void;
    onMouseOver?: (any: any) => void;
    onProjectionChanged?: () => void;
    onResize?: () => void;
    onRightClick?: (any: any) => void;
    onTilesLoaded?: () => void;
    onTiltChanged?: () => void;
    onZoomChanged?: () => void;
    styles?: object;
}
export interface WrappedGmapObj {
    gmaps_obj?: any;
    hover: () => void;
    hovered: boolean;
    hover_options: LooseObject;
    unhover: () => void;
    highlight: () => void;
    highlighted: boolean;
    highlight_options: LooseObject;
    unhighlight: () => void;
    show: () => void;
    hide: () => void;
    remove: () => void;
    _cbs: {
        [key: string]: (any?: any) => void;
    };
    registerEventCB: (cb: (any?: any) => void) => void;
    unRegisterEventCB: (cb: (any?: any) => void) => void;
    options: LooseObject;
    update: (options: any) => Promise<WrappedGmapObj>;
    update_hover: (options: any) => Promise<WrappedGmapObj>;
}
export interface WrappedPolygon extends WrappedGmapObj {
    gmaps_obj: google.maps.Polygon;
}
export interface WrappedPolyline {
    gmaps_obj: google.maps.Polyline;
}
export interface WrappedDirections {
    gmaps_obj: google.maps.DirectionsRenderer;
}
export interface WrappedMarker {
    gmaps_obj: google.maps.Marker;
}
export default class WrappedMapBase extends React.Component<MapBaseProps, any> {
    do_after_init: any;
    do_on_drag_end: any;
    do_on_drag_start: any;
    drawing_completed_listener: any;
    map: any;
    initialized: boolean;
    map_objects: {
        directions: object;
        marker: object;
        polygon: object;
        polyline: {
            [key: string]: WrappedGmapObj;
        };
    };
    highlight_objects: {
        marker: object;
        polygon: object;
        polyline: object;
    };
    cutting_objects: {
        [key: string]: any;
        hover_scissors?: any;
    };
    overlay: google.maps.OverlayView | null;
    cutting: {
        enabled: boolean;
        id: any;
        indexes: any;
        arr?: [][];
    };
    cutting_completed_listener: (any: any) => void;
    helpers: any;
    script_cache: any;
    html_element: any;
    services: any;
    constructor(props: any);
    componentWillMount(): void;
    componentWillUnmount(): void;
    componentDidMount(): void;
    doAfterInit(): void;
    setCenter(latLng: any): Promise<void>;
    fitToBoundsArray(arr_of_arrays: any): Promise<{}>;
    fitToBoundsObjectArray(arr_of_objects: any): Promise<{}>;
    toPixel(lat_lng_input: google.maps.LatLng | LatLngLiteral): [number, number];
    setZoom(zoom_level: any): Promise<void>;
    setPolyline(id: any, options: any, hover_options?: any, highlight_options?: any): Promise<WrappedGmapObj>;
    unsetPolyline(id: any): Promise<boolean>;
    clearPolylines(): Promise<boolean[]>;
    setPolygon(id: any, options: any, hover_options?: any, highlight_options?: any): Promise<WrappedGmapObj>;
    unsetPolygon(id: any): Promise<boolean>;
    clearPolygons(): Promise<boolean[]>;
    setDirections(id: any, options: any, hover_options?: any): Promise<WrappedGmapObj>;
    unsetDirections(id: any): Promise<boolean>;
    clearDirections(): Promise<boolean[]>;
    setMarker(id: any, options: any, hover_options?: any, highlight_options?: any): Promise<WrappedGmapObj>;
    unsetMarker(id: any): Promise<boolean>;
    clearMarkers(): Promise<boolean[]>;
    registerDragEndCB(cb: any): void;
    unregisterDragEndCB(cb: any): void;
    registerDragStartCB(cb: any): void;
    unregisterDragStartCB(cb: any): void;
    setupMapEvents(map: any): void;
    setDrawingMode(type: any, opts: any, cb?: any): void;
    completeDrawingMode(): void;
    cancelDrawingMode(): void;
    setCuttingMode(polyline_id: any, cb?: any): void;
    cuttingPositionUpdate(mouse_event: any): void;
    cuttingClick(mouse_event: any): void;
    completeCuttingMode(): void;
    render(): JSX.Element;
}
export declare function fitBoundsWithPadding(gMap: any, bounds: any, paddingXY: any): void;
export {};
//# sourceMappingURL=index.d.ts.map