"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var counter = 0;
var scriptMap = new Map();
exports.ScriptCache = (function (global) {
    return function ScriptCache(scripts) {
        var Cache = {
            _onLoad: function (key) {
                return function (cb) {
                    var stored = scriptMap.get(key);
                    if (stored) {
                        stored.promise.then(function () {
                            stored.error ? cb(stored.error) : cb(null, stored);
                        });
                    }
                    else {
                        // TODO:
                    }
                };
            },
            _scriptTag: function (key, src) {
                if (!scriptMap.has(key)) {
                    var tag_1 = document.createElement('script');
                    var promise = new Promise(function (resolve, reject) {
                        var body = document.getElementsByTagName('body')[0];
                        tag_1.type = 'text/javascript';
                        tag_1.async = false; // Load in order
                        var cbName = "loaderCB" + counter++ + Date.now();
                        var cleanup = function () {
                            if (global[cbName] && typeof global[cbName] === 'function') {
                                global[cbName] = null;
                            }
                        };
                        var handleResult = function (state) {
                            return function (evt) {
                                var stored = scriptMap.get(key);
                                if (state === 'loaded') {
                                    stored.resolved = true;
                                    resolve(src);
                                    // stored.handlers.forEach(h => h.call(null, stored))
                                    // stored.handlers = []
                                }
                                else if (state === 'error') {
                                    stored.errored = true;
                                    // stored.handlers.forEach(h => h.call(null, stored))
                                    // stored.handlers = [];
                                    reject(evt);
                                }
                                cleanup();
                            };
                        };
                        tag_1.onload = handleResult('loaded');
                        tag_1.onerror = handleResult('error');
                        tag_1.onreadystatechange = function () {
                            handleResult(tag_1.readyState);
                        };
                        // Pick off callback, if there is one
                        if (src.match(/callback=CALLBACK_NAME/)) {
                            src = src.replace(/(callback=)[^&]+/, "$1" + cbName);
                            window[cbName] = tag_1.onload;
                        }
                        else {
                            tag_1.addEventListener('load', tag_1.onload);
                        }
                        tag_1.addEventListener('error', tag_1.onerror);
                        tag_1.src = src;
                        body.appendChild(tag_1);
                        return tag_1;
                    });
                    var initialState = {
                        loaded: false,
                        error: false,
                        promise: promise,
                        tag: tag_1
                    };
                    scriptMap.set(key, initialState);
                }
                return scriptMap.get(key);
            }
        };
        Object.keys(scripts).forEach(function (key) {
            var script = scripts[key];
            Cache[key] = {
                tag: Cache._scriptTag(key, script),
                onLoad: Cache._onLoad(key)
            };
        });
        return Cache;
    };
})(window);
exports.default = exports.ScriptCache;

//# sourceMappingURL=ScriptCache.js.map
