export declare const ScriptCache: (scripts: any) => {
    _onLoad: (key: any) => (cb: any) => void;
    _scriptTag: (key: any, src: any) => any;
};
export default ScriptCache;
//# sourceMappingURL=ScriptCache.d.ts.map